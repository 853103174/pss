/**
 +------------------------------------------------------------------------------------+
 + 后台
 +------------------------------------------------------------------------------------+
 + 作者：ice
 + 官方：www.uzjz.com
 + 时间：2018-04-25
 +------------------------------------------------------------------------------------+
 */
//加载css
ice.loadCss(ice.path+'admin/iceAdmin.css');
ice(function(){
	var sidebarOpen = ice.getCookie('admin-sidebar')?ice.getCookie('admin-sidebar'):'open';
	//获取页面对象
	var sidebar=ice('.admin-sidebar')[0];
	var page = ice('.admin-page')[0];
	var logo = ice('.admin-logo')[0];

	//内容板块自适应屏幕高度
	var sidebarH = sidebar.offsetHeight;
	var headerH = ice('.admin-header')[0].offsetHeight;
	var footerH = ice('.admin-footer')[0].offsetHeight;
	var contentH = ice.web().h-headerH-footerH;
	contentH = contentH<sidebarH?sidebarH-headerH-footerH:contentH;
	ice('.admin-content')[0].style.minHeight=contentH+'px';

	if(sidebarOpen == 'close'){
		if(ice.web().w>768){
			logo.style.width='48px';
		}
		ice.addCss(page,'admin-sidebar-close');
		ice.addCss(sidebar,'close');
		ice('.admin-toggle').addCss('open');
		//ice('.admin-menu').addCss('close');
	}

	//侧栏隐藏展开-收缩
	ice('.admin-toggle')[0].onclick=function(){
		ice.toggleCss(this,'open','close',function(){
			if(ice.web().w>768){
				logo.style.width='48px';
				//设置cookie
				ice.delCookie('admin-sidebar');
				ice.setCookie('admin-sidebar','close');
			}
		},function(){
			logo.style.width='';
			//设置cookie
			ice.delCookie('admin-sidebar');
			ice.setCookie('admin-sidebar','open');
		});
		ice.toggleCss(page,'admin-sidebar-close');
		ice.toggleCss(sidebar,'open','close');
	}

	//侧栏菜单隐藏展开-收缩
	ice('.admin-toggle-right')[0].onclick=function(){
		ice('.admin-menu').toggleCss('open','close');
		ice.toggleCss(this,'open');
	}

	//侧栏菜单展开-收缩
	ice.use('tree',function(){
		ice.tree({
			id:"tree",
			func:function(e){
				var p=e.parentNode;
				if(p.className!=('active-dropdown')){
					if(p.className=='active'){
						p.className='';
					}else{
						p.className='active';
					}
				}
			}
		});
	});

	//菜单树高亮
	var nav = ice('.admin-sidebar a');
	for(var i=0;i<nav.length;i++){
		if(nav[i].href == document.location.href){
			nav[i].parentNode.className="active";
		}else{
			nav[i].parentNode.className="";
		}
	}
});