'use strict';
/**
 * iceTable v1.0.0
 * MIT License By www.iceui.net
 * 作者：ICE
 * ＱＱ：308018629
 * 官网：www.iceui.net
 * 说明：版权完全归iceui所有，转载和使用请注明版权
 * -------------------------------------------------------------
 * 表格排序插件
 * iceTable 2020-06-28
 * options      {json}
 *  ├ el        {object|id}	表格对象或者id，必填
 *  ├ sort 		{string}	需要排序的列索引，从0开始，选填
 *  ├ success	{function}	回调函数
 *  └ func      {function}	确认的回调函数
 */
var ice = ice || {};
ice.table = function(options) {
	options = options || {};
	var el = typeof options.el == 'object' ? options.el : document.getElementById(options.el);
	var sort = options.sort != undefined ? options.sort.split(',') : false;
	var callback = options.success || function() {};
	if (!el || el.length || el.tagName != 'TABLE' || el.rows.length <= 1) return;
	//定义变量
	var rows = [],thead = [],first = [],last = null;
	//获取每行对象
	for (var i = 0; i < el.rows.length; i++) {
		rows.push(el.rows[i]);
	}
	//如果sort参数定义，按照sort初始化，默认每列可自由排序
	thead = rows.shift().cells;
	for (var i = 0; i < (sort ? sort.length : thead.length); i++) {
		var s = sort ? sort[i] : i;
		if (s >= thead.length) continue;
		first[s] = false;
		addCss(thead[s],'table-sort');
		thead[s].innerHTML += '<span class="table-sort-arrow"></span>';
		thead[s].i = s;
		thead[s].onclick = function(){sortFn(this.i)};
	}
	//排序
	function sortFn(col) {
		if (last) {
			delCss(last,'table-sort-asc');
			delCss(last,'table-sort-desc');
		}
		var sortNum = true;
		for (var i = 0; i < rows.length && sortNum; i++) {
			sortNum = /^\d+(\.\d+)?$/.test(rows[i].cells[col].innerHTML);
		}
		rows.sort(function (row1,row2){
			var result;
			var value1, value2;
			value1 = row1.cells[col].innerHTML;
			value2 = row2.cells[col].innerHTML;
			if (value1 == value2) return 0;
			result = sortNum ? parseFloat(value1) > parseFloat(value2) : value1 > value2;
			result = result ? 1 : -1;
			return result;
		})
		if (first[col]) {
			rows.reverse();
			first[col] = false;
			delCss(thead[col],'table-sort-asc');
			addCss(thead[col],'table-sort-desc');
		} else {
			first[col] = true;
			delCss(thead[col],'table-sort-desc');
			addCss(thead[col],'table-sort-asc');
		}
		last = thead[col];
		var frag = document.createDocumentFragment();
		for (var i = 0; i < rows.length; i++) {
			frag.appendChild(rows[i]);
		}
		el.tBodies[0].appendChild(frag);
		callback(thead[col], first[col]);
	}
	//删除class
	function delCss(el, name) {
		var re = new RegExp('\\b' + name + '\\b', 'g');
		if (el.className) {
			el.className = el.className.replace(re, '');
			el.className = el.className.replace(/^\s+|\s+$/g, '').replace(/\s+/g, ' ');
			if (el.className == '') el.removeAttribute('class');
		}
	}
	//添加class
	function addCss(el, name) {
		var re = new RegExp('\\b' + name + '\\b', 'g');
			if (el.className) {
			if (el.className.search(re) == -1) el.className += ' ' + name;
		} else {
			el.className = name;
		}
		el.className = el.className.replace(/^\s+|\s+$/g, '').replace(/\s+/g, ' ');
	}
};
(function(){var a = document.createElement('style');a.type = 'text/css';a.innerHTML = '';document.getElementsByTagName('head')[0].appendChild(a);})();