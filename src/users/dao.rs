use mysql::Value;
use chrono::Local;
use crate::rules::Vo;
use crate::util::{db, cap};

// 查询用户所有的菜单权限
pub fn menus(user_id: u32) -> Vec<Vo> {
    let sql = "select m.id,m.name,m.url,m.parentId,m.types,m.class from menu m 
    inner join users_menu um on um.userId=? and um.menuId=m.id order by m.orders";
    let paras = vec![Value::from(user_id)];
    
    db::list(&sql, &paras)
}

// 根据用户名密码查询人员信息
pub fn get_user(cols: &str, username: &str, password: &str) -> Vo {
    let sql = format!("select {} from users where username=?", cols);
    let mut sql = String::from(&sql);
    let mut paras = vec![Value::from(username)];
    if !password.is_empty() {
        sql.push_str(" and passwords=?");
        paras.push(Value::from(password));
    }
    sql.push_str(" limit 1");
    
    db::get(&sql, &paras)
}

// 根据ID查询人员信息
pub fn user_info(cols: &str, id: &u32) -> Vo {
    let sql = format!("select {} from users where id=? limit 1", cols);
    let sql = String::from(&sql);
    let paras = vec![Value::from(id)];
    
    db::get(&sql, &paras)
}

// 查询人员数量
pub fn user_count(vo: &Vo) -> u32 {
    let len = vo.len();
    let mut paras = Vec::with_capacity(len);
    let mut sql = String::with_capacity(cap(len));
    sql.push_str("select count(1) from users");
    if len > 0 {
        sql.push_str(" where 1=1");
        if vo.not_blank("name") {
            sql.push_str(" and name like ?");
            paras.push(Value::from(format!("{}%", vo.str("name"))));
        }
        if vo.not_blank("statu") {
            sql.push_str(" and statu=?");
            paras.push(Value::from(vo.str("statu")));
        }
        if vo.not_blank("job") {
            sql.push_str(" and job like ?");
            paras.push(Value::from(format!("{}%", vo.str("job"))));
        }
    }
    
    db::count(&sql, &paras)
}

// 查询人员列表
pub fn user_list(cols: &str, vo: &Vo) -> Vec<Vo> {
    let len = vo.len();
    let mut paras = Vec::with_capacity(len);
    let mut sql = String::with_capacity(cap(len));
    sql.push_str(&format!("select {} from users", cols));
    if len > 0 {
        sql.push_str(" where 1=1");
        if vo.not_blank("name") {
            sql.push_str(" and name like ?");
            paras.push(Value::from(format!("{}%", vo.str("name"))));
        }
        if vo.not_blank("statu") {
            sql.push_str(" and statu=?");
            paras.push(Value::from(vo.str("statu")));
        }
        if vo.not_blank("job") {
            sql.push_str(" and job like ?");
            paras.push(Value::from(format!("{}%", vo.str("job"))));
        }
        if vo.not_blank("orderBy") {
            sql.push_str(" order by ");
            sql.push_str(vo.str("orderBy"));
        }
        if vo.not_blank("page") {
            let size = vo.int("size");
            sql.push_str(" limit ");
            sql.push_str(&format!("{}", (vo.int("page") - 1) * size));
            sql.push(',');
            sql.push_str(&format!("{}", size));
        }
    }
    
    db::list(&sql, &paras)
}

// 保存职工信息
pub fn save_users(vo: &Vo) -> u32 {
    let mut map = db::set_params("users", vo);
    if vo.blank("id") {
        map.insert(String::from("createTime"), Value::from(Local::now().naive_local()));
        db::save("users", &map)
    } else {
        let _ = db::update("users", &map);
        vo.long("id")
    }
}

// 查询职工奖惩记录
pub fn user_reward_list(cols: &str, vo: &Vo) -> Vec<Vo> {
    let len = vo.len();
    let mut paras = Vec::with_capacity(len);
    let mut sql = String::with_capacity(cap(len));
    sql.push_str(&format!("select {} from users_reward", cols));
    if len > 0 {
        sql.push_str(" where 1=1");
        if vo.not_blank("userId") {
            sql.push_str(" and userId=?");
            paras.push(Value::from(vo.long("userId")));
        }
        if vo.not_blank("types") {
            sql.push_str(" and types=?");
            paras.push(Value::from(vo.str("types")));
        }
        if vo.not_blank("startDate") {
            sql.push_str(" and rewardDate>=?");
            paras.push(Value::from(vo.str("startDate")));
        }
        if vo.not_blank("endDate") {
            sql.push_str(" and rewardDate<=?");
            paras.push(Value::from(vo.str("endDate")));
        }
    }
    
    db::list(&sql, &paras)
}

// 根据ID查询职工奖惩记录
pub fn user_reward_info(cols: &str, id: &u32) -> Vo {
    let sql = format!("select {} from users_reward where id=? limit 1", cols);
    let sql = String::from(&sql);
    let paras = vec![Value::from(id)];
    
    db::get(&sql, &paras)
}

// 保存职工奖惩记录
pub fn save_user_reward(vo: &Vo) -> u32 {
    let map = db::set_params("users_reward", vo);
    if vo.blank("id") {
        db::save("users_reward", &map)        
    } else {
        let _ = db::update("users_reward", &map);
        vo.long("id")
    }
}
