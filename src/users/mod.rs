pub(crate) mod action;
pub(crate) mod dao;

// 学历
pub fn educations() -> Vec<&'static str> {
    vec![
        "小学",
        "初中",
        "高中",
        "职高",
        "中专",
        "大专",
        "本科",
        "研究生",
        "硕士",
        "博士",
    ]
}

// 奖惩方式
pub fn rewards() -> Vec<&'static str> {
    vec![
        "突出贡献",
        "违纪处罚",
        "口头警告",
        "口头表扬",
        "销售突破",
        "请假扣除",
    ]
}

// 人员状态
pub fn status() -> Vec<&'static str> {
    vec![
        "培训", 
        "实习", 
        "试用", 
        "在职", 
        "病休", 
        "离岗", 
        "离职", 
        "退休",
    ]
}
